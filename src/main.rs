extern crate termion;
use rand::Rng;
use std::{
    fmt::{self, Display},
    fs,
};
use termion::color::{self};

use std::io::stdin;
use termion::input::TermRead;

const ROW_SIZE: usize = 6;

fn main() {
    let word_list = WordList::new();
    let answer_characters = word_list.pick_random_word();
    // let answer_character: Vec<char> = "HAPPY".chars().collect();

    let mut table = Table::new(answer_characters.clone());

    let stdin = stdin();
    let mut stdin = stdin.lock();

    let mut error_message: Option<String> = None;

    loop {
        display(&table, &error_message);

        let pass = stdin.read_line();

        if let Err(err) = pass {
            println!("Error occured!... {:?}", err);
            continue;
        }

        let input_characters = pass.unwrap().expect("取得できませんでした");
        if input_characters.len() != answer_characters.len() {
            error_message = Some(format!("{}文字で入力してください", answer_characters.len()));
            continue;
        }
        if !word_list.is_include_word(&input_characters) {
            error_message = Some(format!("[{}]は存在しません", input_characters));
            continue;
        }

        error_message = None;

        let i = table
            .rows
            .iter()
            .position(|r| !r.is_inputed())
            .expect("can't add word...");

        table.rows[i] = table.rows[i].input(input_characters.chars().collect());

        if table.rows[i].is_all_matched() {
            display(&table, &None);
            println!(
                "{}{}{}{}You are win! 🎊",
                termion::cursor::Goto(1, (4 + ROW_SIZE) as u16),
                color::Fg(color::White),
                termion::style::Bold,
                color::Bg(color::Black),
            );
            break;
        }

        if table.rows.iter().all(|row| row.is_inputed()) {
            display(&table, &None);
            println!(
                "{}{}{}{}You lose... 💀 \nAnswer word is [{}]",
                termion::cursor::Goto(1, (4 + ROW_SIZE) as u16),
                color::Fg(color::White),
                termion::style::Bold,
                color::Bg(color::Red),
                answer_characters.into_iter().collect::<String>()
            );
            break;
        }
    }
}

fn display(table: &Table, error_message: &Option<String>) {
    println!(
        "{}{}{}{}{}{}\nGuesst the word. 🤔",
        termion::clear::All,
        color::Fg(color::White),
        termion::cursor::Goto(1, 1),
        table,
        color::Fg(color::White),
        color::Bg(color::Black),
    );
    println!(
        "{}{}{}{}{}{}",
        termion::cursor::Goto(1, (5 + ROW_SIZE) as u16),
        color::Fg(color::White),
        color::Bg(color::Red),
        error_message.as_ref().unwrap_or(&"".to_string()),
        color::Fg(color::White),
        color::Bg(color::Black),
    );
    println!("{}", termion::cursor::Goto(1, (2 + ROW_SIZE) as u16))
}

struct WordList {
    word_list: Vec<String>,
}

impl WordList {
    fn new() -> WordList {
        let raw_word_list = fs::read_to_string("./5letter_word_list.json")
            .expect("can't find `5letter_word_list.json`.");
        let word_list: Vec<String> =
            serde_json::from_str(&raw_word_list).expect("can't decode wordlist...");
        WordList { word_list }
    }
    fn pick_random_word(&self) -> Vec<char> {
        let mut rng = rand::thread_rng();
        let i = rng.gen_range(0..self.word_list.len());
        return self.word_list[i].to_uppercase().chars().collect();
    }

    fn is_include_word(&self, word: &String) -> bool {
        self.word_list.iter().any(|_word| _word == word)
    }
}

struct Table {
    rows: [Row; ROW_SIZE],
}

impl Table {
    fn new(answer_characters: Vec<char>) -> Table {
        Table {
            rows: [
                Row::new(None, answer_characters.clone()),
                Row::new(None, answer_characters.clone()),
                Row::new(None, answer_characters.clone()),
                Row::new(None, answer_characters.clone()),
                Row::new(None, answer_characters.clone()),
                Row::new(None, answer_characters.clone()),
            ],
        }
    }
}

impl Display for Table {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.rows.iter().fold(Ok(()), |result, row| {
            result.and_then(|_| writeln!(f, "{}", row))
        })
    }
}

struct Cell {
    input_character: Option<char>,
    answer_characters: Vec<char>,
    row_index: usize,
}

impl Cell {
    fn new(input_character: Option<char>, answer_characters: Vec<char>, row_index: usize) -> Cell {
        Cell {
            input_character,
            answer_characters,
            row_index,
        }
    }

    fn is_inputed(&self) -> bool {
        self.input_character.is_some()
    }

    fn is_exactly_match(&self) -> bool {
        if !self.is_inputed() {
            return false;
        }
        let answer_character = self
            .answer_characters
            .get(self.row_index)
            .expect("index is out of vec!");
        if let Some(_input_character) = self.input_character {
            return (*answer_character) == _input_character;
        }

        false
    }

    fn is_misaligned(&self) -> bool {
        if !self.is_inputed() {
            return false;
        }
        if self.is_exactly_match() {
            return false;
        };

        if let Some(_input_character) = self.input_character {
            return self
                .answer_characters
                .iter()
                .any(|c| (*c) == _input_character);
        }

        false
    }
}

impl Display for Cell {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        if !self.is_inputed() {
            return write!(f, "{}□", color::Bg(color::Black));
        }
        let color: Box<dyn color::Color> = if self.is_exactly_match() {
            Box::new(color::Green)
        } else if self.is_misaligned() {
            Box::new(color::Yellow)
        } else {
            Box::new(color::Black)
        };

        let color = color.as_ref();
        color.write_bg(f).and(write!(
            f,
            "{}",
            &self.input_character.expect("入力終わってない")
        ))
    }
}

struct Row {
    input_characters: Option<Vec<char>>,
    answer_characters: Vec<char>,
    cells: Vec<Cell>,
}

impl Row {
    fn is_inputed(&self) -> bool {
        self.input_characters.is_some()
    }

    fn new(input_characters: Option<Vec<char>>, answer_characters: Vec<char>) -> Row {
        if input_characters.is_none() {
            return Row {
                input_characters,
                answer_characters: answer_characters.clone(),
                cells: answer_characters
                    .clone()
                    .iter()
                    .enumerate()
                    .map(|(i, _)| Cell::new(None, answer_characters.clone(), i))
                    .collect(),
            };
        }
        let cells = input_characters
            .clone()
            .expect("入力が終わってない")
            .iter()
            .enumerate()
            .map(|(i, input_character)| {
                Cell::new(Some(*input_character), answer_characters.clone(), i)
            })
            .collect();
        Row {
            input_characters,
            answer_characters,
            cells,
        }
    }

    /**
     * 入力
     */
    fn input(&self, input_characters: Vec<char>) -> Row {
        let uppercase_input_characters: Vec<char> = input_characters
            .into_iter()
            .map(|c| c.to_uppercase().next().expect("aaa"))
            .collect();
        Row {
            cells: uppercase_input_characters
                .clone()
                .into_iter()
                .enumerate()
                .map(|(i, c)| Cell::new(Some(c), self.answer_characters.clone(), i))
                .collect(),
            input_characters: Some(uppercase_input_characters),
            answer_characters: self.answer_characters.clone(),
        }
    }

    fn is_all_matched(&self) -> bool {
        if !self.is_inputed() {
            return false;
        }
        return self.cells.iter().all(|c| c.is_exactly_match());
    }
}

impl Display for Row {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.cells.iter().fold(Ok(()), |result, album| {
            result.and_then(|_| write!(f, "{}", album))
        })
    }
}
